﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MortazaviTest.DataAccess.Models;
using MortazaviTest.Businesss;
using System;
using MortazaviTest.Business.ViewModels;

namespace MortazaviTest.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PeopleController : ControllerBase
    {
        private readonly PeopleBusiness _peopleBusiness;

        public PeopleController(PeopleBusiness peopleBusiness)
        {
            _peopleBusiness = peopleBusiness;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<PeopleViewModel>>> GetPeoples()
        {
            return await _peopleBusiness.GetAll();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<PeopleViewModel>> GetPeople(int id)
        {
            var people = await _peopleBusiness.GetById(id);

            if (people == null)
                return NotFound();

            return people;
        }

        [HttpPut]
        public async Task<IActionResult> PutPeople(PeopleViewModel people)
        {

            var result = await _peopleBusiness.Update(people);

            if (result == null)
                return NotFound();

            return Ok(people);
        }

        [HttpPost]
        public async Task<ActionResult<PeopleViewModel>> PostPeople(PeopleViewModel people)
        {
            var result = await _peopleBusiness.Create(people);

            return result;
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> DeletePeople(int id)
        {
            try
            {
                await _peopleBusiness.Delete(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);

            }
        }


    }
}
