USE [MortazaviTestDb]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Peoples](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Birthday] [datetime2](7) NOT NULL,
	[Config] [nvarchar](500) NOT NULL,
 CONSTRAINT [PK_Peoples] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO


/************************************ Stored Procedures *********************************/

/*** Insert ***/

CREATE PROCEDURE [dbo].[people_insert]

	@Name varchar(50),
	@Birthday date,
	@Config varchar(max)

AS
BEGIN

INSERT INTO [dbo].[Peoples]
           ([Name]
           ,[Birthday]
           ,[Config])
     VALUES
           (@Name,
		   @Birthday,
		   @Config)
END
GO



/*** Update ***/

CREATE PROCEDURE [dbo].[people_update] 
	@id int,
	@Name varchar(50),
	@Birthday date,
	@Config varchar(max)
AS
BEGIN
UPDATE [dbo].[Peoples]
   SET [Name] = @Name
      ,[Birthday] = @Birthday
      ,[Config] = @Config
 WHERE [id]=@id

END
GO


/*** Select All ***/

CREATE PROCEDURE [dbo].[people_SelectAll]

AS
BEGIN

SELECT * FROM [dbo].[Peoples]

END
GO



/*** Select by Id ***/

CREATE PROCEDURE [dbo].[people_SelectById]

@id int

AS
BEGIN

SELECT * FROM [dbo].[Peoples]
where Id=@id

END
GO



/*** Delete ***/

Create PROCEDURE [dbo].[people_delete]
@id int

AS
BEGIN

	SET NOCOUNT ON;
	Delete from [dbo].[Peoples]
	where Id=@id

END
GO


