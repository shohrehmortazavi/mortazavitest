﻿using Microsoft.EntityFrameworkCore;
using MortazaviTest.DataAccess.Models;

namespace MortazaviTest.DataAccess
{
    public class MortazaviTestDbContext : DbContext
    {
        public MortazaviTestDbContext(DbContextOptions options) : base(options)
        { }

        public DbSet<People> Peoples { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {

            base.OnModelCreating(builder);
            builder.Entity<People>().Property(p => p.Name).IsRequired().HasMaxLength(50);
            builder.Entity<People>().Property(p => p.Birthday).IsRequired();
            builder.Entity<People>().Property(p => p.Config).IsRequired().HasMaxLength(500);         
    }

    }
}
