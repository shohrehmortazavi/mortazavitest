﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace MortazaviTest.DataAccess
{
    public interface IRepository<T> : IDisposable where T : class
    {
        Task<IQueryable<T>> ExecuteSPGetAll(string spQuery, List<object> parameters);
        Task<int> ExecuteSPPost(string spQuery,List<object> parameters = null);
        Task<T> ExecuteSPGetById(string spQuery, List<object> parameters = null);
       
        
        T GetById(int id);
        IEnumerable<T> GetAll();
        IEnumerable<T> Find(Expression<Func<T, bool>> expression);
        void Add(T entity);
        void AddRange(IEnumerable<T> entities);
        void Remove(T entity);
        void RemoveRange(IEnumerable<T> entities);
    }
}
