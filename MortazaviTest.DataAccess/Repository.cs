﻿using Microsoft.EntityFrameworkCore;
using MortazaviTest.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace MortazaviTest.DataAccess
{
    public class Repository<T> : IRepository<T> where T : class
    {


        private readonly MortazaviTestDbContext _context;

        public Repository(MortazaviTestDbContext context)
        {
            _context = context;
        }

        public async Task<IQueryable<T>> ExecuteSPGetAll(string spQuery, List<object> parameters = null)
        {
            if (parameters == null) parameters = new List<object>();
            return _context.Set<T>().FromSqlRaw(spQuery, parameters.ToArray());
        }

        public async Task<T> ExecuteSPGetById(string spQuery, List<object> parameters = null)
        {
            if (parameters == null) parameters = new List<object>();
            return _context.Set<T>().FromSqlRaw(spQuery, parameters.ToArray()).AsEnumerable().FirstOrDefault();
        }

        public async Task<int> ExecuteSPPost(string spQuery, List<object> parameters = null)
        {
            if (parameters == null) parameters = new List<object>();
            var result = _context.Database.ExecuteSqlRaw(spQuery, parameters.ToArray());
            return result;
        }

        public void Add(T entity)
        {
            _context.Set<T>().Add(entity);
        }

        public void AddRange(IEnumerable<T> entities)
        {
            _context.Set<T>().AddRange(entities);
        }

        public void Dispose()
        {
            _context.Dispose();
        }

        public IEnumerable<T> Find(Expression<Func<T, bool>> expression)
        {
            return _context.Set<T>().Where(expression);
        }

        public IEnumerable<T> GetAll()
        {
            return _context.Set<T>().ToList();
        }

        public T GetById(int id)
        {
            return _context.Set<T>().Find(id);
        }

        public void Remove(T entity)
        {
            _context.Set<T>().Remove(entity);
        }

        public void RemoveRange(IEnumerable<T> entities)
        {
            _context.Set<T>().RemoveRange(entities);
        }
    }
}
