﻿using AutoMapper;
using Microsoft.Data.SqlClient;
using MortazaviTest.Business.ViewModels;
using MortazaviTest.DataAccess;
using MortazaviTest.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MortazaviTest.Businesss
{
    public class PeopleBusiness
    {
        private readonly IRepository<People> _peopleRepository;
        private readonly IMapper _mapper;

        public PeopleBusiness(IRepository<People> peopleRepository, IMapper mapper)
        {
            _peopleRepository = peopleRepository;
            _mapper = mapper;
        }


        public async Task<List<PeopleViewModel>> GetAll()
        {
            var result = await _peopleRepository.ExecuteSPGetAll("EXEC people_SelectAll", null);
            return result.AsEnumerable().Select(m => _mapper.Map<PeopleViewModel>(m)).ToList();
        }


        public async Task<PeopleViewModel> GetById(int peopleId)
        {
            var people = await _peopleRepository.ExecuteSPGetById("EXEC people_SelectById @id", CreateParameters(new PeopleViewModel() { Id = peopleId }));

            if (people == null)
                throw new Exception("people not found!");

            return _mapper.Map<PeopleViewModel>(people);

        }


        public async Task<PeopleViewModel> Create(PeopleViewModel people)
        {
            try
            {
                if (people.Id > 0)
                    throw new Exception("people Id is not null!");

                var result = await _peopleRepository.ExecuteSPPost("EXEC people_insert @Name, @Birthday, @Config", CreateParameters(people));

                if (result > 0)
                    return people;
                else
                    throw new Exception("people not saved in Database!");
            }
            catch (Exception)
            {
                throw;
            }
        }


        public async Task<PeopleViewModel> Update(PeopleViewModel people)
        {
            if (people.Id == 0)
                throw new Exception("people not found! Id is 0;");

            try
            {
                await _peopleRepository.ExecuteSPPost("EXEC people_update @Id,@Name,@Birthday,@Config", CreateParameters(people));
                return people;
            }
            catch (Exception)
            {
                throw;
            }
        }


        public async Task Delete(int peopleId)
        {
            try
            {
                var result = await _peopleRepository.ExecuteSPPost("EXEC people_Delete @Id", CreateParameters(new PeopleViewModel() { Id = peopleId }));
            }
            catch (Exception)
            {

                throw;
            }
        }


        private List<object> CreateParameters(PeopleViewModel people)
        {
            var parameters = new List<object>();

            if (people.Id != 0)
                parameters.Add(new SqlParameter("@id", people.Id));
            if (!string.IsNullOrEmpty(people.Name))
                parameters.Add(new SqlParameter("@Name", people.Name));
            if (people.Birthday != null)
                parameters.Add(new SqlParameter("@Birthday", people.Birthday));
            if (!string.IsNullOrEmpty(people.Config))
                parameters.Add(new SqlParameter("@Config", people.Config));
            return parameters;
        }

    }
}
