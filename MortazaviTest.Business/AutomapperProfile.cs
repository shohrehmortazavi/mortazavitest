﻿using AutoMapper;
using MortazaviTest.Business.ViewModels;
using MortazaviTest.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace MortazaviTest.Business
{
    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            CreateMap<People, PeopleViewModel>().ReverseMap();
        }
    }
}
