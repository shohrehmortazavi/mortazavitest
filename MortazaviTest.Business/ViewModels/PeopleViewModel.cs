﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MortazaviTest.Business.ViewModels
{
    public class PeopleViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime? Birthday { get; set; }
        public string Config { get; set; }
    }
}
